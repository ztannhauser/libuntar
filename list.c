
#include <assert.h>

#include <debug.h>

#include "tar/list.h"

#include "list.h"

static int (*listers[number_of_archive_types])(int fd) = {
	[at_tar] = libuntar_tar_list,
};

int libuntar_list(
	int fd,
	enum archive_type type)
{
	int error = 0;
	int (*lister)(int);
	ENTER;
	
	dpv(fd);
	
	lister = listers[type];
	
	assert(lister);
	
	error = lister(fd);
	
	EXIT;
	return error;
}

