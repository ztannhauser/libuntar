
#include <assert.h>
#include <string.h>
#include <stddef.h>

#include <debug.h>

#include <enums/error.h>

#include "archive_type.h"
#include "match_suffix.h"

int libuntar_match_suffix(
	const char* path,
	enum archive_type* out)
{
	int error = 0;
	ENTER;
	
	assert(path);
	
	dpvs(path);
	
	size_t len = strlen(path);
	
	if (len >= 4 && !strcmp(path + len - 4, ".tar"))
		*out = at_tar;
	else if (len >= 3 && !strcmp(path + len - 3, ".ar"))
		*out = at_ar;
	else
		error = e_unknown_suffix;
	
	EXIT;
	return error;
}



















