
#include <strings.h>
#include <time.h>
#include <stdio.h>
#include <pwd.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/types.h>
#include <stdint.h>

#include <debug.h>

#include <defines/argv0.h>

#include <enums/error.h>

#include "header.h"
#include "list.h"

int libuntar_tar_list(int fd)
{
	int error = 0;
	uint8_t block[512];
	
	struct tar_header* header = (typeof(header)) block;
	
	ssize_t read_retval;
	
	char* m;
	mode_t mode;
	uid_t uid;
	gid_t gid;
	size_t size;
/*	time_t mtime;*/
	unsigned checksum;
	struct passwd* passwd;
	
	char line[4096];
	
	ENTER;
	
	bool pax_extended_header;
	bool keep_going = true;
	while (!error && keep_going)
	{
		pax_extended_header = false;
		
		if ((read_retval = read(fd, &block, sizeof(block))) < 0)
			error = e_syscall_failed;
		else if (!read_retval)
		{
			keep_going = false;
		}
		else if (read_retval < sizeof(block))
		{
			fprintf(stderr, "%s: read(): %m\n", argv0);
			error = e_syscall_failed;
		}
		else if (!memcmp(block, (uint8_t[512]){}, 512))
		{
			HERE;
			keep_going = false;
		}
		else if (errno = 0, mode = strtoul(header->mode, &m, 8), errno || !index(" ", *m))
		{
			HERE;
			error = e_malformed_archive;
		}
		else if (errno = 0, uid = strtoul(header->uid, &m, 8), errno || !index(" ", *m))
		{
			HERE;
			error = e_malformed_archive;
		}
		else if (errno = 0, gid = strtoul(header->gid, &m, 8), errno || !index(" ", *m))
		{
			HERE;
			error = e_malformed_archive;
		}
		else if (errno = 0, size = strtoul(header->size, &m, 8), errno || !index(" ", *m))
		{
			HERE;
			error = e_malformed_archive;
		}
		#if 0
		else if (errno = 0, mtime = strtoul(header->mtime, &m, 8), errno || !index(" ", *m))
		{
			HERE;
			error = e_malformed_archive;
		}
		#endif
		else if (errno = 0, checksum = strtoul(header->checksum, &m, 8), errno || !index(" ", *m))
		{
			HERE;
			error = e_malformed_archive;
		}
		else if (({
			size_t i, n;
			signed ssum;
			unsigned usum;
			signed char *sc = (typeof(sc)) block;
			unsigned char *uc = (typeof(uc)) block;
			
			memset(&header->checksum, ' ', sizeof(header->checksum));
			
			for (ssum = 0, usum = 0, i = 0, n = 512; i < n; i++)
				ssum += sc[i], usum += uc[i];
			
			(checksum != ssum && checksum != usum);
		}))
		{
			HERE;
			error = e_malformed_archive;
		}
		else switch (header->file_type[0]) {
			case '\0':
			case '0': mode |= S_IFREG; break;
			case '1': mode |= S_IFREG; break;
			case '2': mode |= S_IFLNK; break;
			case '3': mode |= S_IFCHR; break;
			case '4': mode |= S_IFBLK; break;
			case '5': mode |= S_IFDIR; break;
			case '6': mode |= S_IFIFO; break;
			case '7': mode |= S_IFREG; break;
			case 'g':
				pax_extended_header = true;
				break;
			default:
				error = e_malformed_archive; break;
		}
		
		if (!error && keep_going && !pax_extended_header)
		{
			m = line;
			
			switch (mode & S_IFMT)
			{
				case S_IFSOCK: *m++ = 's'; break;
				case S_IFLNK:  *m++ = 'l'; break;
				case S_IFREG:  *m++ = '-'; break;
				case S_IFBLK:  *m++ = 'b'; break;
				case S_IFDIR:  *m++ = 'd'; break;
				case S_IFCHR:  *m++ = 'c'; break;
				case S_IFIFO:  *m++ = 'p'; break;
			}
			
			*m++ = mode & 00400 ? 'r' : '-';
			*m++ = mode & 00200 ? 'w' : '-';
			*m++ = mode & 00100 ? 'x' : '-';
			*m++ = mode & 00040 ? 'r' : '-';
			*m++ = mode & 00020 ? 'w' : '-';
			*m++ = mode & 00010 ? 'x' : '-';
			*m++ = mode & 00004 ? 'r' : '-';
			*m++ = mode & 00002 ? 'w' : '-';
			*m++ = mode & 00001 ? 'x' : '-';
			
			*m++ = ' ';
			
			if ((passwd = getpwuid(uid)))
				m = stpcpy(m, passwd->pw_name);
			else
				m += sprintf(m, "%u", uid);
			
			*m++ = ' ';
			
			if ((passwd = getpwuid(gid)))
				m = stpcpy(m, passwd->pw_name);
			else
				m += sprintf(m, "%u", gid);
			
			*m++ = ' ';
			
			m += sprintf(m, "%8zu ", size);
			
			#ifdef SYSTEM_LIBC
			m += strftime(m, 512, "%m/%d/%y ", localtime(&mtime));
			#endif
			
			m = stpcpy(m, header->name);
			
			*m = '\0', puts(line);
		}
		
		if ((size % 512))
			size += 512 - (size % 512);
		
		if (!error && lseek(fd, size, SEEK_CUR) < 0)
		{
			dperror(errno);
			error = e_syscall_failed;
		}
	}
	
	EXIT;
	return error;
}

























