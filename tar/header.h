
// https://en.wikipedia.org/wiki/Tar_(computing)#File_format

struct tar_header
{
	char name[100];
	char mode[8];
	char uid[8];
	char gid[8];
	char size[12];
	char mtime[12];
	char checksum[8];
	char file_type[1];
	char link_path[100];
	
	// ustar extension:
	char ustar_indicator[6];
	char version[2];
	char uname[32];
	char gname[32];
	char dmajor[8];
	char dminor[8];
	char file_prefix[155];
};
