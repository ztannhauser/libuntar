
CC = gcc

CFLAGS += -Werror -Wall

buildtype ?= release

ifeq ($(buildtype), release)
CPPFLAGS += -D DEBUGGING=0

CFLAGS += -O2
CFLAGS += -flto
else
CPPFLAGS += -D DEBUGGING=1

CFLAGS += -g
CFLAGS += -Wno-unused-variable
CFLAGS += -Wno-unused-function
CFLAGS += -Wno-unused-but-set-variable
endif

default: gen/libtar.a

.PRECIOUS: %/

%/:
	mkdir -p $@

gen/srclist.mk: | gen/
	find -name '*.c' | sed 's/^/srcs += /' > $@

include gen/srclist.mk

objs = $(patsubst %.c,gen/$(buildtype)/%.o,$(srcs))
deps = $(patsubst %.c,gen/$(buildtype)/%.d,$(srcs))

gen/libtar.a: $(objs)
	ar r $@ $^

gen/$(buildtype)/%.o: %.c | gen/$(buildtype)/%/
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@

















